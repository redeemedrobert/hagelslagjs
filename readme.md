## HagelslagJS
#### The tastiest, ultra-light, reactive, and routable JavaScript DOM framework.


HagelslagJS is an _ultra-light_ JavaScript DOM framework that aims to be as small as possible and as simple as possible, while including the JS framework functionality that has become essential to modern web development; with NO dependencies! The TypeScript file that the framework is contained in, unminified and uncompressed, is only 20kb; that's 5kb smaller than this readme!

_**Hagelslag**_ is a Dutch delicacy, it is essentially chocolate sprinkles, which are commonly sprinkled ontop of buttered bread.
_**HagelslagJS**_ is a JavaScript framework that (just like the effect of hagelslag on bread), makes your web development process sweeter, without the heaviness associated with your average JavaScript frameowrk that offers the same features.
The philosophy behind HagelslagJS is that JavaScript is already a good, powerful language, and doesn't need to be reinvented by the framework you use for web development. HTML is similarly unrivaled in its power to create user interfaces. So then, HagelslagJS doesn't impose any restrictive demands on the way you write JavaScript, HTML, and CSS, but conveniently links them all together in your JavaScript, leveraging their built-in power and eloquence, while giving it the power of modularity and reactivity.

_**Hagelslag**_ is an easy, simple, lightweight way to make your buttered bread **INCREDIBLE.**  
_**HagelslagJS**_ is an easy, simple, lightweight way to make HTML/JS/CSS projects **INCREDIBLE** _(and reactive)._  

What better way is there to show its potential than with an example to-do list? The following code puts out this fully functional to-do list:
![alt text](example.png "Hagelslag.js to-do list example")
(The "Change button color" button just changes the background color of those two buttons to demonstrate reactive class assignment. The Home and About pages are shown to illustrate routing.)

index.html:
```
<html>
    <head>
        <title>Hagelslag Test To-do</title>
        <link rel="stylesheet" href="/HagelslagJS/teststyle.css" />
        <script type="module" src="/HagelslagJS/testmain.js"></script>
    </head>
    <body>
        <div id="title-div">Hagelslag To Do Test!</div>
        <div id="app-root"></div>
    </body>
</html>
```
main.ts:
```
import { Hagel, Boter } from './hagelslag.js'
import { domTree, toDoList, about, fourohfour } from './dom.js'

const app = new Hagel('app-root')
app.a(Hagel.c(domTree))

export const router = new Boter('app-container', [
    { route: '', element: ()=>{ return Hagel.c(toDoList) }, default: true },
    { route: 'about', element: ()=>{ return Hagel.c(about) }},
    { route: 'var/$number/notvar/$letter', element:()=> { return Hagel.c(toDoList) }},
    { route: '404', element: ()=> { return Hagel.c(fourohfour) }, notfound: true }
], true, 'HagelslagJS')
```
state.ts:
```
import { Slag } from './hagelslag.js'

export const state = new Slag()

state.ra('toDoList', [{text: 'Buy some hagelslag', completed: false}, {text: 'Buy some bread', completed: true}, {text: 'Buy some butter', completed: true}, {text: 'Put the butter on the bread and then the hagelslag on the butter', completed: false}, {text: 'Consume the treat and experience unprecedented levels of gezellig', completed: false}])
state.rv('bg', 'black')
```
dom.ts:
```
import { Hagel, ElementProperties } from './hagelslag.js'
import { state } from './state.js'
import { router } from './main.js'
import * as F from './functions.js'

export const domTree:ElementProperties = {
    id: 'app',
    tagName: 'div',
    children: [
        Hagel.c({ id: 'nav', tagName: 'div', children: [
            Hagel.c({ tagName: 'a', innerText: 'Home | ', events: [ 'click', ()=>router.navigate('') ]}),
            Hagel.c({ tagName: 'a', innerText: 'About', events: [ 'click', ()=>router.navigate('about') ]})
        ]}),
        Hagel.c({ id: 'app-container', tagName: 'div' })
    ]
}

export const toDoList = { id: 'to-do-container', tagName: 'div', children: [
    Hagel.c({ id: 'to-do-list', tagName: 'ol', $for: 'toDoList',
        template: (value)=> {
            return Hagel.c({ tagName: 'li',
                children: [
                    Hagel.c({ tagName: 'input',
                        props: [ 'type', 'checkbox', ...(value.completed ? ['checked', ''] : []) ],
                        events: ['change', ()=> F.checkedChange(value)]
                    }),
                    Hagel.c({ tagName: 'span', props: [...(value.completed ? ['class', 'completed'] : [])], innerText: value.text }),
                    Hagel.c({ tagName: 'input', props: ['type','button', 'value', 'X'], events: ['click', ()=> F.deleteItem(value)] })
                ]
            })
        }
        }, state),
        Hagel.c({ id: 'add-todo-input', tagName: 'input', props: [ 'type', 'text', 'placeholder', 'New todo title' ] }),
        Hagel.c({ id: 'add-todo-button', tagName: 'button', innerText: 'Add',
        $classes: [{ name: 'bg', className: 'redbg', condition: 'red' }],
        events: [ 'click', ()=>F.addItem() ]
        }, state),
        Hagel.c({ id: 'change-todo-button', tagName: 'button', innerText: 'Change Button Color!',
        $classes: [{ name: 'bg', className: 'redbg', condition: 'red' }],
        events: [ 'click', ()=>state.uv('bg', state.gv('bg') === 'red' ? 'black' : 'red') ]
    }, state)
]}

export const about: ElementProperties = { id: 'about', tagName: 'div', 
    innerHTML: `You're looking at the HagelslagJS to-do example about page.<br />
    If this were a real application, I'd put a nice description here.<br />
    All I wanted to do was show the router in action though, so this is what you get.<br />
    Helaas pindakaas!`
}

export const fourohfour: ElementProperties = { id: 'fourohfour', tagName: 'div', innerHTML: '404 :-(' }
```
functions.ts:
```
import { state } from './state.js'

export function checkedChange(value): void {
    state.ua('toDoList', state.ga('toDoList').map(item=>{
        if(item.text === value.text){
            return { text: value.text, completed: !value.completed }
        } else return item
    }))
}

export function deleteItem(value): void {
    state.ua('toDoList', state.ga('toDoList').filter(item=>item.text !== value.text))
}

export function addItem(): void {
    state.ua('toDoList', [ ...state.ga('toDoList'), {completed: false, text: (document.getElementById('add-todo-input') as HTMLInputElement).value}]);
    (<HTMLInputElement>document.getElementById('add-todo-input')).value = ''
}
```
style.css:
```
body {
    margin: 0;
    font-family: Arial;
    background-color: black;
}
ol {
    background-color: #005000;
    margin: 0;
}
li {
    color: white;
    padding: 5px 0 5px 0;
}
li:nth-child(odd) {
    background-color: #003000;
}
li:nth-child(even) {
    background-color: #004000;
}
li input[type=button] {
    float: right;
    margin-right: 10px;
}
#title-div {
    background-color: darkgreen;
    color: white;
    text-align: center;
    padding: 50px;
    margin: 0;
    font-size: 40px;
    font-weight: bolder;
}
#nav {
    color: white;
    text-align: center;
    background-color: green;
}
#nav a, #nav a:hover {
    cursor: pointer;
}
.completed {
    text-decoration: line-through;
    color: gray;
}
.redbg {
    background-color: red;
    color: white;
}
#fourohfour {
    color: white;
    font-size: 30px;
}
#about {
    padding: 30px;
    margin: 0;
    background-color: #003000;
    color: white;
}
```

#### Documentation
###### Basics
1. The DOM is managed by a class called `Hagel`. It is instantiated with a single argument, which is a string that will be the ID of the HTML element that the Hagelslag DOM will be appended to.
    1. Example:
    ```
    import { Hagel } from './hagelslag.js'

    const app = new Hagel('app-root')
    ```
    `app` will be used to reference our Hagel instance from this point on.
1. Reactivity is managed by a class called `Slag`. It is instantiated without any argument, but it is important that it is instantiated at the very beginning of your code, so that its reactive variables are available before the DOM that depends on it is rendered.
    1. Example:
    ```
    import { Slag } from './hagelslag.js'

    const state = new Slag()
    ```
    `state` will be used to reference our Slag instance from this point on.
1. Routing is managed by a class called `Boter`. An application can have as many routers as you'd like, wich allows for routers within routers within routers... You can choose whether or not you wish for a given router to modify the page URL or not, which keeps the routing within routing within routing tidy. Each router will be an individual `Boter` instance in the application.
    1. `Boter` is instantiated with multiple arguments:
        1. `outlet: string` is the ID of an HTML element that will serve as the outlet for the router being instantiated.
        1. `routes?:Array<{route:string, element:(data?:any)=>Element, default?:boolean, notfound?:boolean}>` is a JavaScript array of objects, each object being a single route that can be navigated to in the router being instantiated. `routes` is optional because routes can be added or removed after instantiation if desired.
            1. `route: string` is the URL for a route, for instance: `route: '/app/profile'`. Routes can contain variables, which are indicated by prefixing a $ to that part of the route, as in: `route: '/app/profile/$userid/photos/$photoid'`. A single route can have an unlimited amount of variables in its URL. The varaibles can be accessed as discussed next. `route` can be an empty string, but this is likely only useful for home pages, which is discussed below with `home:boolean`.
            1. `element:(data?:any)=>Element` is the element that will be placed in the router when the route is navigated to. `data?:any` is a JavaScript object that contains all of the variables from the route, if any; the variables are passed into the function so that `Element` can be rendered using the data from the variables. Example: for the route `/app/profile/$userid/photos/$photoid`, if the actual URL navigated to is `/app/profile/13453/photos/152`, the data object will look like this: `{ userid: '13453', photoid: '152' }`, so that when constructing `Element`, `$userid` and `$photoid` can be accessed with `data.userid` and `data.photoid` to be processed as needed to construct `Element` as desired.
            1. `home:boolean` should be set to `true` if the route is to be the "home" route that is navigated to if no route is specified when the application is navigated to, as in if the user navigates to `yourwebsite.com` instead of `yourwebsite.com/yourroutehere`. You can set `home` to `true` in as many routes as you like, but only the first one will actually be navigated to. You can leave `route` blank if the route is the homepage, so that no route path gets pushed to the URL/browser history if a user ever nagivates to it via a link or something along those lines.
            1. `notfound?:boolean` is the route that will be used in the case of a 404 if `notfound` is set to `true`. If no `notfound` route is specified, HagelslagJS will just put a generic '404, page not found' div into the router outlet.
        1. `changeURL?:boolean` determines whether or not this router will affect the page's URL/the browser history. If set to true, when a route is navigated to, the browser's URL will be updated with the route; if set to false, the route will be navigated to and the page's URL and the browser history will remain as they were before navigation.
        1. `basePath?:string` is a string that is used if the application has any path leading up to it that should be considered when calculating routes and changing the page URL and browser history. For instance, if your application is at `yourwebsite.com/your/app/is/here`, `basePath` will be `your/app/is/here` and then HagelslagJS will include that in the URL and browser history when changing either, and will know not to include it in route calculation when the page is navigated to. When `basePath` is set, routes do **NOT** need to have basePath prefixed to them; so at `yourwebsite.com/your/app/is/here`, your routes will still just be `route: '/app/profile'` or `route: '/app/profile/$userid/photos/$photoid'`, which would yield `yourwebsite.com/your/app/is/here/app/profile` or `yourwebsite.com/your/app/is/here/app/profile/12345/photos/123` as long as `basePath` is set to `your/app/is/here`.
###### Building a DOM
1. The DOM is constructed using the commands `Hagel.createElement()` and `app.append`. Most all Hagelslag commands also have shorthand, in this case, `Hagel.c()` and `app.a()`.
    1. `Hagel.createElement()` (and `Hagel.c()`) are static functions, which means that they cannot be called from an instance of Hagel, but must be called in reference to the class itself, which is why it is not written here as `app.createElement()`.
        1. `Hagel.createElement()` accepts one two parameters, `props`, which is a JavaScript Object containing these properties: 
        ```
        interface ElementProperties {
            tagName: string,
            id?: string,
            props?: Array<string>,
            children?: Array<Element>,
            innerHTML?: string,
            innerText?: string,
            $innerHTML?: string,
            $innerText?: string,
            $classes?: Array<{name: string, className: string, condition: any}>,
            events?: Array<any>,
            for?: Array<any>,
            $for?: string,
            template?: (item: any) => Element
        }
        ```
        and `state`, which is a `Slag` instance which is _ONLY_ needed if the `Hagel.createElement()` function will be leveraging app state and reactivity, as will be described later. This parameter can be ignored if the element being created will not be reactive.
        1. `tagName` is the name of the HTML tag that will be used to create the HTML element. This is the only required property. Examples would be `div` or `input`.
        1. `id` is the ID that will be assigned to the HTML object; that is, the ID referenced by `document.getElementById()`.
        1. `props` is a JavaScript Array of HTML properties that will be assigned to the element, such as `fontsize` or `value`. The Array is read in order as so: `[ propertyName, propertyValue, propertyName, propertyValue ]`, or as a practical example: `[ 'fontsize', 10, 'value', 'This is an example value!' ]`.
        1. `children` is a Javascript Array of HTML elements that will be appended, in the order they appear in the array, to the element that will be created by the `Hagel.createElement()` that the current ElementProperties object is being fed to. See the dom.ts example above for an example of this in action.
        1. `innertHTML` is a string that will be set as the HTML element's innerHTML. By design, this property is, in reality, applied to the element BEFORE the `children` property, otherwise all child elements would be wiped out by this property being set.
        1. `innerText` is the same as `innerHTML`, except, per the functionality of JavaScript/HTML, any string passed in which contains HTML will not be rendered as HTML, but plain text; innerText renders everything as plain text.
        1. All properties that begin with a $ are reactive properties, and they will be addressed later on, when reactivity is addressed.
        1. `events` is an array which will be consumed by `Hagel.createElement()` to assign functions to the various 'on' events supported by JavaScript. The format of the array should be: `[ onEventStringName, onEventFunction, onEventStringName, onEventFunction ] `, or practically: `[ 'keyup', ()=>alert('a key was released!'), 'keydown', ()=>alert('A key was pressed!') ]`
        1. `for` is an array of any type, the elements of which will be iterated through and passed into the `template` property (see below) to produce an element which will be appended to the element being created by the `Hagel.createElement()` function that is currently consuming this ElementProperties object.
        1. `template` is a function that returns an element, which will be appended to the element that `Hagel.createElement()` is currently creating per the array given in the `for` property. The function must accept one argument, which is a single array element from the `for` array that is passed to `template` while all of its elements are being iterated through. During that iteration, each array element from `for` is passed into `template` to be turned into an HTML element and then appended to the HTML element being created by `Hagel.createElement()`.
        1. An element created with `Hagel.createElement` can be appended to the application using `app.append()`. This appends the element using `insertAdjacentElement` at the position `beforeend`, in order to avoid disrupting any JavaScript that is already attached to the application.
1. Application state and reactivity is managed using one or more instances of `Slag`. You can have as many instances of `Slag` as you like, giving you the ability to have very complex, reactive application states.
    1. Application state is created using the commands `state.registerVariable()` (or `state.rv()`), `state.registerArray()` (or `state.ra()`), and `state.registerClass()` (or `state.rc()`). Application state is consumed for reactivity either using functions, or procedurally during the create of elements with `Hagel.createElement()`, the latter being the most practical. Both will be explained in detail later.
        1. `state.registerVariable()` accepts two parameters: a string which will be the name of the reactive variable, and the value of the variable, which can be anything. The variable can be updated using `state.updateVariable()` or `state.uv()`, which accepts the same parameters, which would be a string to tell the function which variable to update, and the value to update that variable to. When a reactive variable is updated, all HTML elements that are bound to it will be updated to reflect the variable change.
        1. The value of a registered variable can be read using `state.getVariable()` (or `state.gv()`), which simply return the value of that variable.
        1. `state.registerArray()` accepts two parameters: a string which will be the name of the reactive array, and an array which will be the value of that array. A reactive array can be updated with `state.updateArray()` or `state.ua()` , which accepts the same parameters plus an optional 'index' parameter. The first argument must be a string, which tells the function the name of the array to update. Then, a new array or a single array element can be given. If an 'index' is given, which must be an integer, then the array or single array element will replace whatever is in the current array at the given index; otherwise, the whole array will be replaced by the array or single array element passed into the function. When a reactive array is updated, all HTML elements bound to it will be updated to reflect the new array, but only the child HTML elements that correspond to one of the array elements that has changed will be updated.
        1. The value of a registered array can be read using `state.getArray()` (or `state.ga()`), which simply returns the array.
        1. `state.registerClass()` accepts four parameters, `name: string, className: string, condition: any, id: string`, and is used to bind reactive class assignment to a reactive variable and an element. `name` is the name of the reactive variable to bind the reactive class assignement to. `className` is the name of the class that will be reactively assigned to the desired element. `condition` is the value of the variabled named by `name` that must be met in order to assign the class from `className` to the desired element. `id` is the ID of the HTML element that will have the class from `className` reactively assigned to it whenever the variable named in `name` is equal to the value of `condition`. An infinite number of reactive class assignments can be bound to any variable and any element, one being bound to a single reactive variable and single element per execution of `state.registerClass()`. These can also be bound during `Hagel.createElement()`, which will be described next.
    1. The most practical means of leveraging application state for reactivity is to use $ properties while creating an HTML element with `Hagel.createElement()`. The properties that can be used to make an HTML element reactive are: `props?: Array<string>, $innerHTML?: string, $innerText?: string, $classes?: Array<{name: string, className: string, condition: any}>, $for?: string, template: (item:any)=>Element`. **NOTE: the `Hagel.createElement()` function will fail and produce an error if no `Slag` instance is given as the second argument of the function, as described earlier in the `Hagel.createElement()` instructions.**
        1. HTML element properties can be assigned to an HTML element as described above with DOM creation using `Hagel.createElement()`. Any valid HTML element property can be made reactive by prefixing its name with a $, as in: `props: [ '$fontsize', 'nameOfAReactiveVariable', '$value', 'nameOfADifferentReactiveVariable' ]`. As is implied in that example, the value of the property should be the name of a reactive variable that was registered with the `state.registerVariable()` function to the application state given as a parameter to the `Hagel.createElement()` function.
        1. The HTML Element's innerHTML can be made reactive by passing the name of a registered reactive variable from the given application state to the `$innerHTML` parameter.
        1. the HTML Element's innerText can be made reactive by passing the name of a registered reactive variable from the given application state to the `$innerText` parameter.
        1. Reactive class assignment can be bound to the HTML element using the `$classes` property. This property accepts an array of objects structured like so: `{name: string, className: string, condition: any}`. `name` is the name of the registered reactive variable that the reactive class assignment will be bound to. `className` is the name of the HTML/CSS class that will be assigned reactively. `condition` is what the value of the given reactive variable must be for the given className to be assigned. Any time that the value of the given reactive variable does not equal the value of condition, the className will be removed from the HTML element IF it is currently assigned. If there are no classNames assigned to the Element, then the className property is removed from the element to keep it tidy.
        1. `$for` functions exactly the same as the non reactive version of `for` detailed above, except instead of passing an array in, you pass the name of a reactive array registered to the given application state. The same `template` property, as also detailed above, is used to create the function template that each array item is passed in to to create the HTML element that is appended to the element being created by `Hagel.createElement()`.
    1. If preferred or needed, application state can also be leveraged for reactivity using the same `Slag` functions that are used internally to create reactivity while using the `Hagel.createElement()` function.
        1. An element property can be made reactive using `state.registerElement()` (or `state.re()`), which accepts three parameters: `id: string, prop: string, name: string`. `id` is the HTML ID of the element to be made reactive; `prop` is the HTML property of the identified element that will be made reactive, and `name` is the name of the reactive variable that is being bound to.
        1. An array can be bound to an HTML element for reactive rendering using `state.registerArrayElement` (or `state.rae()`), which accepts three parameters: `id: string, template: (item:any)=>Element, variable: string`. `id` is the ID of the HTML element that the output of each Array element will be appended to, `template` is a function which takes in an array element and outputs an HTML element which will be appended to the given HTML element with the given ID, and `variable` is the name of the reactive array that is being bound to.
        1. Reactive class assignment can be accomplished with `state.registerClass()` (or `state.rc()`), the usage of which is detailed in the directions for managing application state above.
    ###### Routing
    1. Once a `Boter` is instantiated as was described in the basic instructions, its routes can be navigated to, routes can be added, or routes can be removed.
        1. `BoterInstance.navigate(route: string): void` will route to the element provided by the route added with the route specified by `route`. If this route does not exist, Hagelslag will throw an error.
        1. `addRoute(route: {route: string, element: (data?:any)=>Element, home?:boolean, notfound?:boolean}): void` will add a route to the router, exactly the same as it would if the `route` object was given in the `routes` array that `Boter` can be instantiated with.
        1. `removeRoute(route: string)` will remove the route that was added with the route specified by `route`. If `route` does not exist on this router, Hagelslag will throw an error.
#### In the works:
1. Nothing at the moment. Enjoy using HagelslagJS!